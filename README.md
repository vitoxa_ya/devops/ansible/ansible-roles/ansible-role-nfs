Ansible-role-nfs
=========

Роль для установки и настройки NFS сервера/клиента. \
Также предусмотренна возможность установки AutoFS на клиент

Role Variables
--------------

#### Переменные для установки пакетов:
  
      nfs_server_pack_name: "nfs-kernel-server"
      nfs_server_pack_state: "present" 
      nfs_client_pack_name: "nfs-common"
      nfs_client_pack_state: "present"
      nfs_client_mount_pack_name: "autofs"
      nfs_client_mount_pack_state: "present"
  ---
  
#### Переменные сервера для расшаренных папок
  
      nfs_server_shares:
        - path: "/files"
          path_state: "directory" # absent
          host: "*" # пример: 10.10.10.10 или 10.10.10.10/24 или 10.10.10.10/255.255.255.0 или web01.ru - если есть запись в /etc/hosts или настроен DNS.
          permissions: "rw,no_root_squash,no_subtree_check,sync" 
          owner: "root"
          group: "root"
          mode: "0644"
  ___
#### Переменные клиента
  
      nfs_client_auto_mount: "" # fstab или autofs или оставить пустым тагда сработает стандартный mount
  ___
      nfs_client_mount:
        - path: "" # 192.168.1.1:/data или web01.ru:/data
          path_state: "directory" # absent
          dest: "" # /mnt
          params: "user,rw,noauto" 
          timeout: "60" # задержка для отмонтирования в autofs

Example Playbook
----------------


    - hosts: servers
      roles:
        - role: 
          - username.rolename
          vars:
          - nfs_server_shares:
            - path: "/files"
              path_state: "directory"
              host: "10.10.10.10" 
              permissions: "rw,no_root_squash,no_subtree_check,sync"
            - path: .....
              ........
          tags:
            - nfs_server
      
    - hosts: client
      roles:
        - role: username.rolename
          vars:
            nfs_client_mount:
              - path: "web01.ru:/data" 
                path_state: "directory"
                dest: "" # /mnt
                params: "user,rw,noauto"
              - path: ......
                ........
            nfs_client_auto_mount: "autofs"
          tags:
            - nfs_client

---
Список тэгов для запуска отдельных задач
------
#### Сервер
___________
  | Tag | Описание |
  |:-|:-|
  | nfs_server | Запуск всех тасок установки пакета и создания папок |
  | nfs_server_package | Запуск таски установки пакета |
  | nfs_server_shares_dir | Запуск таски создания папок |

#### Клиент
___________
  | Tag | Описание |
  |:-|:-|
  | nfs_client | Запуск всех тасок установки пакета, создания папок и запись настроек монтирования в fstab |
  | nfs_client_package | Запуск таски установки пакета |
  | nfs_client_folder | Запуск таски создания папок |
  | nfs_client_mount_fstab | Запись настроек монтирования в fstab |
  | nfs_client_package_autofs | Запуск таски установки пакета autofs |
  | nfs_client_mount_autofs | Запись настроек монтирования |
    
License
-------

MIT

Author Information
------------------

vitoxaya
